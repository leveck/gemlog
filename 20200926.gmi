# Sharp PC1261

Years ago I bought a Sharp PC1261 pocket computer. It was manufactured in 1984, has a 8-bit CMOS SC61860 ESR-H 768 KHz CPU, and 10kb of RAM. At the same time I also bought a new in box Sharp Printer and Cassette Interface CE-123P. I messed around with the pocket computer, but never unboxed the cassette interface. I wrote a handy little Game Master Emulator (a simple random oracle) in BASIC on the device and did not wat to lose the program, so it just sat on a shelf unless I was using it for RPG play.

The videos flying around YouTube for #septandy (the TRS80 pocket computers were also made by Sharp) made me think, "why did I never just save that GME program to tape?" So tonight I dug it out, loaded up fresh batteries in it and my tape recorder, and did just that. While I was at it I recorded the program to wav via audacity and uploaded it
=> gopher://1436.ninja/1Tapes/ here.
I will be placing other programs there later on as I screw around with this thing in the future.
