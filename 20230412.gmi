# Long week continued

⏱ 20230412
♬ Slayer - South of Heaven

## Cincinnati to Utah

This morning I awoke at 0500, drove to the Cincinati airport and was there shortly after 0600. The usual rental return, check in, etc.

=>gemini://1436.ninja/gemlog/images/20230412a.jpg

A flight to Denver where I had an entire row to myself!

=>gemini://1436.ninja/gemlog/images/20230412b.jpg

The secret vaping at the airport -- no one makes eyecontact with strangers in an airport...

=>gemini://1436.ninja/gemlog/images/20230412c.jpg

Waiting on my *delayed* (of course) flight to Salt Lake City. There I will rent another car and drive to Vernal for an action packed three days at a plant followed by a frantic drive home to fly the next morning. Never a dull moment.
